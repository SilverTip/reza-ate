#REZA ATE RXX.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-08-25

code_rev = 3
#Version Log:
#------------------------------
#R1.0 -(20210825) - Ported over code from LNB ATE2 R1.py and ATE2_Datasheet_Generator.py - Benjamin Stadnik
#R2.0 -(20210826) - Updated Amplitude response function to include points rather than max in range
#R3 - (20210924) - Added exception management for sceenshots, NFG analysis, Datasheet generation. Created .exe file for tamper-proofing

import pyvisa as visa
import os
import time
import datetime
import math
import shutil
from PIL import Image
import xlsxwriter
import sys


## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.93::hislip0::INSTR'              #MXA 9020B Spectrum Analyzer
path = os.path.dirname(__file__)
settings = {'nf_gain_interested_start_Frequency': 950000000, 'nf_gain_interested_stop_Frequency': 2000000000}

def main():
    #Try to connect to instrument
    connect_flag = False
    while(connect_flag == False):
        try:
            Equipment_Init()
            connect_flag = True
        except Exception as e:
            print(e)
            print('Cannot connect to NFA.')
            SA_VISA = input('Please enter NFA VISA address: (Hint: Keysight Connection Expert)')

    #Determine freq range of unit
    print('')
    print('Frequency range of analysis:')
    print('Start Freq: ' + str(settings['nf_gain_interested_start_Frequency']))
    print('Stop Freq: ' + str(settings['nf_gain_interested_stop_Frequency']))
    print('')
    command = input('Would you like to change this range? (Y/N)')
    if command == 'Y' or command == 'y':
        settings['nf_gain_interested_start_Frequency'] = int(input('Start frequency: (MHz)')) * 1000000
        settings['nf_gain_interested_stop_Frequency'] = int(input('Stop frequency: (MHz)')) * 1000000
    else:
        print('Default frequencty range selected')
        print('')
    

    #Begin Testing
    tester = input('Enter tester name:')
    while(True):
        SN = input('Enter unit serial number:')

        #Gather headings
        data = {}
        data['Tester'] = tester
        data['SN'] = SN
        data['date'] = str(datetime.datetime.now().date())   
        data['time'] = str(datetime.datetime.now().time())                    
        
        #try:
            #Gather data
        data['NFG'] = get_NFG(settings, SN) 
            #Generate Datasheet
        Datasheet(data)        
        #except Exception as e:
            #print(e)
            #print('Something went wrong. Please try again')

def SA_Screenshot(productNumber):
    #SA.write(":DISPlay:FSCReen:STATe 0") 
    SA.write(":DISPlay:FSCReen:STATe 1") 
    filename = productNumber + '.PNG'    
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "\\\SVR-1\\Newmore\\" 
    #saving a screenshot on the equipment
    try:
        SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")
        time.sleep(1)
        ## Move screenshots to proper folder
        shutil.move(SA_save_path + filename, path + '\\' + filename)
    except Exception as e:
        print(e)
        print('Screenshot not found. Please re-map \\\\SVR-1\\Newmore as N: to the NFA and desktop computer')
        pass   

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
           #print('waiting...')
   except KeyboardInterrupt:
       print('Keyboard Interrupt Detected.')
       raise Exception
  
def get_NFG(settings, name): 
    
    NFG = {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : None,'amplitude_response_120MHz' : None,'amplitude_response_500MHz' : None,'amplitude_response_1000MHz' : None,'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None}
    
    #Start NFG Sweep
    SA.write('*CLS')
    SA.write(":DISPlay:FSCReen:STATe 0")
    SA.write(":DISPlay:FSCReen:STATe 1")
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('INITiate:RESTart')
    Wait(SA)

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    number_of_points = float(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])

    #Saving frequency data
    NFG['Frequency'] = []
    NFG['Frequency'].append(freq_start)
    add_next_Freq=((freq_stop-freq_start)/(number_of_points-1))
    i = 1
    next_Frequency = freq_start
    while i < number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        NFG['Frequency'].append(next_Frequency)
        i += 1   
    NFG['Frequency'].append(freq_stop)

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG['Frequency']):
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_start_Frequency'])) and flag == 0:
            interested_freq['start'] = i
            flag = 1
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_stop_Frequency'])):
            interested_freq['stop'] = i
            break
        i += 1
    print(interested_freq['start'], interested_freq['stop'])
    
    #Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG['NF'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    i = interested_freq['start']
    interested_NF = []
    while i <= interested_freq['stop']:
        interested_NF.append(NFG['NF'][i])
        i += 1
    #print(interested_NF)
    NFG['min_NF'] = min(interested_NF)
    NFG['max_NF'] = max(interested_NF)
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq['start']
    LinearNF = []
    while i <= interested_freq['stop']:
        LinearNF.append(10**(NFG['NF'][i]/10))
        i += 1
    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG['averageNF'] = 10 * math.log10(averageLinearNF)

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG['Gain'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq['start']
    interested_Gain = []
    while i <= interested_freq['stop']:
        interested_Gain.append(NFG['Gain'][i])
        i += 1
    NFG['min_Gain'] = min(interested_Gain)
    NFG['max_Gain'] = max(interested_Gain)
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq['start']
    LinearGain = []
    while i <= interested_freq['stop']:
        LinearGain.append(10**(NFG['Gain'][i]/10))
        i += 1
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG['averageGain'] = 10 * math.log10(averageLinearGain)
    
    #Gather gain ripple per bandwidth step
    try:
        NFG['amplitude_response_10MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 10000000)
        NFG['amplitude_response_10MHz_max'] = max(NFG['amplitude_response_10MHz'])
        NFG['amplitude_response_120MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 120000000)
        NFG['amplitude_response_120MHz_max'] = max(NFG['amplitude_response_120MHz'])
        NFG['amplitude_response_500MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 500000000)
        NFG['amplitude_response_500MHz_max'] = max(NFG['amplitude_response_500MHz'])
        NFG['amplitude_response_1000MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 1000000000)
        NFG['amplitude_response_1000MHz_max'] = max(NFG['amplitude_response_1000MHz']) 
    except:
        pass #amplitude range may or may not exist
    
    SA_Screenshot(name)
    
    return NFG

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def Equipment_Init(): 
    global rm, SA
    rm = visa.ResourceManager()
    SA = rm.open_resource(SA_VISA)
    SA.timeout = 10000
    print('Connected to ' + str(SA.query('*IDN?')))

def Datasheet(datastore):
    workbook = xlsxwriter.Workbook(datastore['SN'] +'.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_landscape()
    bold = workbook.add_format({'bold': True})
    not_bold = workbook.add_format({'bold': False})
    bold.set_font_size(8)
    not_bold.set_font_size(8)
    
    #Write unique data
    worksheet.write('A1','Tester:',bold)
    worksheet.write('A2','Unit SN:',bold)
    worksheet.write('D1','Date:',bold)
    worksheet.write('D2','Time:',bold)
    worksheet.write('G1','Script Rev:',bold)
    
    worksheet.write('B1',datastore['Tester'],not_bold)
    worksheet.write('B2',datastore['SN'],not_bold)
    worksheet.write('E1',datastore['date'],not_bold)
    worksheet.write('E2',datastore['time'],not_bold)
    worksheet.write('H1',code_rev,not_bold)
    
    worksheet.write('G6','Amplitude Response, Max',bold)

    #Frequency	Noise 	Gain	Per 10 MHz	Per 120 Mhz	Per 500 MHz  
    worksheet.write('A4','Max',bold)
    worksheet.write('A5','Average',bold)
    worksheet.write('A6','Min',bold)
    
    #Headings
    worksheet.write('A8','Frequency',bold)
    worksheet.write('B8','Noise',bold)
    worksheet.write('C8','Gain',bold)
    worksheet.write('D8','Linear Gain',bold)
    worksheet.write('E8','Linear NF',bold)
    worksheet.write('G8','Per 10 MHz',bold) 
    worksheet.write('H8','Per 120 MHz',bold)
    worksheet.write('I8','Per 500 MHz',bold)
    worksheet.write('J8','Per 1000 MHz',bold)

    #Write Data
    row = 9
    Ripple_Start = {'10':None, '120':None,'500':None,'1000':None}
    lenx = len(datastore['NFG']['Frequency'])
    for i in range(row,row+lenx):
        tmp = "A"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet.write(tmp,datastore['NFG']['Frequency'][tmp_index],not_bold)
        if(datastore['NFG']['Frequency'][tmp_index] == settings['nf_gain_interested_start_Frequency'] + 10000000):
            Ripple_Start['10'] = tmp_index
        if(datastore['NFG']['Frequency'][tmp_index] == settings['nf_gain_interested_start_Frequency'] + 120000000):
            Ripple_Start['120'] = tmp_index
        if(datastore['NFG']['Frequency'][tmp_index] == settings['nf_gain_interested_start_Frequency'] + 500000000):
            Ripple_Start['500'] = tmp_index
        if(datastore['NFG']['Frequency'][tmp_index] == settings['nf_gain_interested_start_Frequency'] + 1000000000):
            Ripple_Start['1000'] = tmp_index
            

    lenx = len(datastore['NFG']['NF'])
    ##WRITE Noise
    for i in range(row,row+lenx):
        tmp = "B"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet.write(tmp,datastore['NFG']['NF'][tmp_index],not_bold)
        
    lenx = len(datastore['NFG']['Gain'])
    ##WRITE GAIN
    for i in range(row,row+lenx):
        tmp = "C"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet.write(tmp,datastore['NFG']['Gain'][tmp_index],not_bold)

    ##AVERAGES  
    worksheet.write_formula('D5','=10^(C5/10)',not_bold)
    worksheet.write_formula('E5','=10^(B5/10)',not_bold)
    worksheet.write('B5',datastore['NFG']['averageNF'],not_bold)
    worksheet.write('C5',datastore['NFG']['averageGain'],not_bold)
    
    ##MINIMUM
    worksheet.write_formula('D6','=10^(C6/10)',not_bold)
    worksheet.write_formula('E6','=10^(B6/10)',not_bold)
    worksheet.write('B6',datastore['NFG']['min_NF'],not_bold)
    worksheet.write('C6',datastore['NFG']['min_Gain'],not_bold)
    
    ##MAXIMUM
    worksheet.write_formula('D4','=10^(C4/10)',not_bold)
    worksheet.write_formula('E4','=10^(B4/10)',not_bold)
    worksheet.write('B4',datastore['NFG']['max_NF'],not_bold)
    worksheet.write('C4',datastore['NFG']['max_Gain'],not_bold)
    worksheet.write('G7',datastore['NFG']['amplitude_response_10MHz_max'],not_bold)
    worksheet.write('H7',datastore['NFG']['amplitude_response_120MHz_max'],not_bold)
    worksheet.write('I7',datastore['NFG']['amplitude_response_500MHz_max'],not_bold)
    worksheet.write('J7',datastore['NFG']['amplitude_response_1000MHz_max'],not_bold)

    ##Write GAIN LINEAR
    for i in range(row,row+lenx):
        tmp = "D"
        tmp2 = "C"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet.write_formula(tmp,formula,not_bold)


    ##Write NF Linear
    for i in range(row,row+lenx):
        tmp = "E"
        tmp2 = "B"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet.write_formula(tmp,formula,not_bold)

    #Locate starting Freq
    
        
    ##Write per 10mhz
    for i in range(len(datastore['NFG']['amplitude_response_10MHz'])):
        tmp = "G"
        tmp += str(i+row+Ripple_Start['10'])
        worksheet.write(tmp,datastore['NFG']['amplitude_response_10MHz'][i],not_bold)

    ##Write per 120mhz
    for i in range(len(datastore['NFG']['amplitude_response_120MHz'])):
        tmp = "H"
        tmp += str(i+row+Ripple_Start['120'])
        worksheet.write(tmp,datastore['NFG']['amplitude_response_120MHz'][i],not_bold)
        
    ##Write per 500mhz
    for i in range(len(datastore['NFG']['amplitude_response_500MHz'])):
        tmp = "I"
        tmp += str(i+row+Ripple_Start['500'])
        worksheet.write(tmp,datastore['NFG']['amplitude_response_500MHz'][i],not_bold)
        
    ##Write per 1000mhz
    for i in range(len(datastore['NFG']['amplitude_response_1000MHz'])):
        tmp = "J"
        tmp += str(i+row+Ripple_Start['1000'])
        worksheet.write(tmp,datastore['NFG']['amplitude_response_1000MHz'][i],not_bold)
        
    try: 
        image1 = path + '\\' + datastore['SN'] + '.PNG'
        worksheet.insert_image('L3', image1 , {'x_scale': 0.75, 'y_scale': 0.75})     
    except:
        print('Image not found')
        pass

    workbook.close()
    os.remove(path + '\\' + datastore['SN'] + '.PNG')



main()

